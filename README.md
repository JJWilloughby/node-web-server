# node-web-server
Simple node/express webserver

## Installation
- Clone repo
- Navigate to root of project and `npm install`

## Usage
- Run server with `node server.js`
- Open a browser and navigate to `localhost:3000`

Additionally, the project is deployed on Heroku and can be accessed by the url:
- https://quiet-everglades-21557.herokuapp.com/

## Purpose
This project was a way for me to get better acquainted with serving files using express, and learning how to deploy my node applications with heroku.
